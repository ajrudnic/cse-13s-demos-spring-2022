// STANDARD INPUT/OUTPUT
#include <stdio.h>
#include <stdbool.h>


bool isvowel(int c) {
  return (c == 'a' || 
          c == 'e' || 
          c == 'i' || 
          c == 'o' || 
          c == 'u');
}

int upper(int c) {
  return (c - 'a') + 'A';
}


int main(void) {
  FILE *infile;
  FILE *outfile;

  // fopen is the function we're going to use
  infile = fopen("fanfic.txt", "r");
  outfile = fopen("new_fanfic.txt", "w");

  int c;
  while((c = fgetc(infile)) != EOF) {
    if (isvowel(c)) {
      fputc(upper(c), outfile);
    } else {
      fputc(c, outfile);
    }
  }

  fclose(infile);
  fclose(outfile);
  return 0;
}
