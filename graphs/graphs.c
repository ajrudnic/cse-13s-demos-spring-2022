#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

typedef struct {
  int vertices;  // the set V
  int **matrix;  // the set E
} Graph;

typedef struct LLint {
  int val;
  struct LLint *next;
} LLint;


void print_items(LLint *node) {
  LLint *here = node;
  while(here) {
    printf("item: %d\n", here->val);
    here = here->next;
  }
}

LLint* enqueue(LLint *node, int val) {
  // TODO: build this
  if (!node) {
    LLint* out = (LLint*)calloc(1, sizeof(LLint));
    out->val = val;
    return out;
  }
  // otherwise, walk the list until we find the end
  LLint *here = node;
  while(here->next != NULL) {
    here = here->next;
  }
  LLint* newnode = (LLint*)calloc(1, sizeof(LLint));
  newnode->val = val;
  here->next = newnode;
  return node;
}

int dequeue(LLint **node) {
  if (!node || !(*node)) {
    return -1;
  }

  int out = (*node)->val;
  LLint *freethis = *node;
  *node = (*node)->next;

  free(freethis);
  return out;
}

bool set_contains(LLint *set, int val) {
  if (!set) {
    return false;
  }
  if (set->val == val) {
    return true;
  }
  return set_contains(set->next, val);
}

LLint *add_to_set(LLint *set, int val) {
  if (set_contains(set, val)) {
    return set;
  } else {
    LLint *out = (LLint *)calloc(1, sizeof(LLint));
    out->next = set;
    out->val = val;
    return out;
  }
}

Graph *graph_create(int vertices) {
  Graph *g = (Graph *)malloc(sizeof(Graph));
  g->vertices = vertices;
  // allocate the outer array.
  g->matrix = (int **)calloc(vertices, sizeof(int*));
  for (int i=0; i < vertices; i++) {
    // allocate each inner array
    g->matrix[i] = (int *)calloc(vertices, sizeof(int));
  }
  return g;
}

void graph_add_edge(Graph *g, int i, int j) {
  g->matrix[i][j] = 1;
}

bool graph_has_edge(Graph *g, int i, int j) {
  return g->matrix[i][j];
}

bool graph_has_path_bfs(Graph *g, int start, int end) {
  // keep track of a set of places that we have visited
  LLint *visited = NULL;

  // keep a queue of places that we want to visit
  LLint *to_visit = NULL;

  to_visit = enqueue(to_visit, start);

  // while I have more places to visit:

  while(to_visit != NULL) {
    //    get the next place to visit
    int current = dequeue(&to_visit);
    printf("hello from lovely vertex %d\n", current);
    
    //    see if it's where we're going (= j)
    if (current == end) {
      return true;
    }

    add_to_set(visited, current);
    // if it's not, enqueue all of its neighbors that have not been visited
    // for each vertex j , check if there's an edge from current->j
    for (int j = 0; j < g->vertices; j++) {
      if (graph_has_edge(g, current, j)) {
        if (!set_contains(visited, j)) {
          to_visit = enqueue(to_visit, j);
        }
      }
    }
  }
  // return false if we're out of places
  return false;
}

int main(void) {
  Graph *g = graph_create(10);

  graph_add_edge(g, 0, 1);
  graph_add_edge(g, 1, 7);
  graph_add_edge(g, 7, 2);
  graph_add_edge(g, 7, 3);
  graph_add_edge(g, 4, 5);

  // printf("has an edge from 1 to 7? %d\n", graph_has_edge(g, 1, 7));
  // printf("has an edge from 1 to 8? %d\n", graph_has_edge(g, 1, 8));

  LLint *queue = NULL;
  queue = enqueue(queue, 12);
  queue = enqueue(queue, 20);
  queue = enqueue(queue, 32);

  bool path_exists;
  path_exists = graph_has_path_bfs(g, 0, 3);
  printf("path exists from 0 to 3? %d\n", path_exists);

  path_exists = graph_has_path_bfs(g, 0, 5);
  printf("path exists from 0 to 5? %d\n", path_exists);

  path_exists = graph_has_path_bfs(g, 0, 9);
  printf("path exists from 0 to 9? %d\n", path_exists);

  path_exists = graph_has_path_bfs(g, 9, 9);
  printf("path exists from 9 to 9? %d\n", path_exists);

  path_exists = graph_has_path_bfs(g, 0, 0);
  printf("path exists from 0 to 0? %d\n", path_exists);

  return 0;
}
