#include <stdio.h>
#include <stdlib.h>

typedef struct ll_int {
  int val;
  struct ll_int *next;
} ll_int;

void print_list(ll_int *node) {
  if (!node) {
    return;
  }
  printf("value: %d\n", node->val);
  print_list(node->next);
}

int main(void) {

  ll_int *front = (ll_int *)malloc(sizeof(ll_int));
  ll_int *second = (ll_int *)malloc(sizeof(ll_int));

  front->val = 4;
  front->next = second;

  second->val = 20;
  second->next = NULL;

  print_list(front);

  free(front);
  free(second);

  // use-after-free bug
  // front->val = 100;

  return 0;
}
