#include <stdio.h>

// the declaration.

int main(void) {

  if (true) {
    int add_thirteen(int x);
  }

  int my_favorite_number = 10;

  printf("memory location of my_favorite_number: %p\n", &my_favorite_number);

  int sum = add_thirteen(my_favorite_number);
  printf("%d\n", sum);

  return 0;
}

// the definition.
int add_thirteen(int x) {
  printf("memory location of x: %p\n", &x);
  return x + 13;
}
