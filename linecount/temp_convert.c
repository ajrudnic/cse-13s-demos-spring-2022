#include <stdio.h>

int main(void) {
  printf("ohai\n");

  int fahrenheit;
  fahrenheit = 62;

  float number;
  double othernumber;

  double celsius = 19;

  for (int fahrenheit = 0; fahrenheit < 100; fahrenheit = fahrenheit + 5) {
    double celsius = (fahrenheit - 32.0) / 9 * 5;
    printf("F: %d\tC: %.1f\n", fahrenheit, celsius);
  }

  printf("C: %.1f\n", celsius);

  return 0;
}
