#include <stdio.h>

int main(void) {
  int num_lines = 0;
  int num_vowels = 0;
  int non_vowels = 0;
  int num_spaces = 0;

  // do some stuff to count the lines
  int c;

  // get the character from stdin
  while ((c = getchar()) != EOF ) {
    // this is how you write a single character in C. with single quotes.
    if (c == '\n') {
      num_lines++;
    }

    // a e i o u
    // A E I O U


    switch(c) {
      case 'a':
      case 'e':
      case 'i':
      case 'o':
      case 'u':
      case 'A':
      case 'E':
      case 'I':
      case 'O':
      case 'U': {
        num_vowels++;
        break;
      }
      case ' ':
        num_spaces += 1;
        break;
      default:
        non_vowels += 1;
        break;
    }
    
    // output the character to stdout
    putchar(c);
  }

  printf("I saw this many lines: %d\n", num_lines);

  printf("I saw this many vowels: %d\n", num_vowels);
  printf("I saw this many non-vowels: %d\n", non_vowels);
  printf("I saw this many spaces: %d\n", num_spaces);

  return 0;
}
