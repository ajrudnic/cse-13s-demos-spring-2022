#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>


typedef struct FriendNode {
  char *name;   // the key
  char *fav_food;  // values
  int shoe_size; // values
  struct FriendNode *next;
} FriendNode;

typedef struct FriendTable {
  // the actual buckets
  // an array of linked lists.
  FriendNode **buckets;
  // num_buckets
  size_t num_buckets;
} FriendTable;


// Thank you Dan Bernstein.
unsigned long hash(char *str) {
  unsigned long hash = 5381;
  int c;

  while (*str != '\0') {
    c = *str;
    hash = ((hash << 5) + hash) + (unsigned char)c; /* hash * 33 + c */
    str++;
  }
  return hash;
}

FriendNode *add_friend_to_list(char *name, char *food, FriendNode *bucket) {
  FriendNode* new_friend;

  new_friend = calloc(1, sizeof(FriendNode));
  new_friend->name = strdup(name);
  new_friend->fav_food = strdup(food);
  new_friend->shoe_size = 20;
  new_friend->next = bucket;

  return new_friend;
}

void add_friend_to_table(char *name, char *food, FriendTable *table) {
  unsigned long hashvalue = hash(name);
  size_t which_bucket = hashvalue % table->num_buckets;

  FriendNode *linkedlist = table->buckets[which_bucket];
  table->buckets[which_bucket] = add_friend_to_list(name, food, linkedlist);
  return;
}

char *fav_food_for_friend(char *name, FriendTable *table) {
  unsigned long hashvalue = hash(name);
  unsigned long which_bucket = hashvalue % table->num_buckets;

  FriendNode *here = table->buckets[which_bucket];

  while(here) {
    if (!strcmp(here->name, name)) {
      return here->fav_food;
    }
    here = here->next;
  }
  return "(friend not found)";
}

FriendNode* delete_friend_from_list(char *name, FriendNode *linkedlist) {
  if (!linkedlist) {
    return NULL;
  } else if (!strcmp(name, linkedlist->name)) {
    // match!!

    // need to free this node!
    FriendNode *next = linkedlist->next;
    // now delete this node.
    free(linkedlist->name);
    free(linkedlist->fav_food);
    free(linkedlist);
    return next;
  } else {
    // recursive case!!
    linkedlist->next = delete_friend_from_list(name, linkedlist->next);
    return linkedlist;
  }
}

bool unfriend(char *name, FriendTable *table) {
  unsigned long hashvalue = hash(name);
  size_t which_bucket = hashvalue % table->num_buckets;

  FriendNode *linkedlist = table->buckets[which_bucket];

  bool found = false;
  FriendNode *here = linkedlist;
  while(here) {
    if (!strcmp(here->name, name)) {
      found = true;
      break;
    }
    here = here->next;
  }
  if (!found) {
    return false;
  } 

  table->buckets[which_bucket] = delete_friend_from_list(name, linkedlist);
  return true;
}


FriendTable *build_friend_table(size_t num_buckets) {
  FriendTable* output = calloc(1, sizeof(FriendTable));
  output->num_buckets = num_buckets;

  // allocate space for num_buckets FriendNode pointers.
  // THEY ARE ALREADY NULL POINTERS.
  output->buckets = calloc(num_buckets, sizeof(FriendNode *));

  return output;
}

void delete_friend_table(FriendTable *table) {
  // for each bucket, delete everything in that bucket
  for (int i=0; i < table->num_buckets; i++) {
    FriendNode *here = table->buckets[i];
    while(here) {
      free(here->name);
      free(here->fav_food);
      FriendNode *freethis = here;
      here = here->next;
      free(freethis);
    }
  }
  free(table->buckets);
  free(table);
}

int main(void) {

  FriendTable* table = build_friend_table(3);

  add_friend_to_table("lucy", "pineapple", table);
  add_friend_to_table("david", "tacos", table);
  add_friend_to_table("adam", "apples", table);
  add_friend_to_table("timmy", "strawberries", table);
  add_friend_to_table("helena", "hummus", table);

  printf("fav food for lucy: %s\n", fav_food_for_friend("lucy", table));
  printf("fav food for timmy: %s\n", fav_food_for_friend("timmy", table));
  printf("fav food for adam: %s\n", fav_food_for_friend("adam", table));
  printf("fav food for bimmy: %s\n", fav_food_for_friend("bimmy", table));


  bool success;
  printf("fav food for david: %s\n", fav_food_for_friend("david", table));
  success = unfriend("david", table);
  printf("succeeded in unfriending? %d\n", success);

  printf("fav food for david: %s\n", fav_food_for_friend("david", table));

  success = unfriend("david", table);
  printf("succeeded in unfriending? %d\n", success);

  delete_friend_table(table);

  return 0;
}
