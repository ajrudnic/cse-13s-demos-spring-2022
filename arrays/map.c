#include <stdio.h>


void map(int (*funk)(int), int *input, int *output, int len) {
  for (int i=0; i < len; i++) {
    output[i] = funk(input[i]);
  }
}

int square(x) {
  return x * x;
}

void square_these_numbers(int *input, int *output, int len) {
  map(square, input, output, len);
}

int add1(x) {
  return x + 1;
}


void add_1_to_these_numbers(int *input, int *output, int len) {
  map(add1, input, output, len);
}

int main(void) {
  int numbers[] = {4, 7, 100, 19, 5, 420};
  int output[6];
  int len = 6;

  square_these_numbers(numbers, output, len);
  for(int i =0; i < len; i++) {
    printf("input: %d, output: %d\n", numbers[i], output[i]);
  }

  add_1_to_these_numbers(numbers, output, len);
  for(int i =0; i < len; i++) {
    printf("input: %d, output: %d\n", numbers[i], output[i]);
  }
}
