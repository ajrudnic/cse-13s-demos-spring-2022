#include <stdio.h>
#include <string.h>

int main(void) {

  // make it so we can change it. if you don't strdup, this will segfault.
  char *message;
  
  message = strdup("'J' is the letter of the day");

  char *othermessage;

  othermessage = message;

  message[1] = 'M';

  printf("%p\n", message);
  printf("%p\n", othermessage);

  printf("%s\n", othermessage);

  return 0;
}
