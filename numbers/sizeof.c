#include <stdio.h>
#include <stdint.h>

int main(void) {

  printf("how big is a char? %lu\n", sizeof(char));

  printf("how big is an int? %lu\n", sizeof(int));

  printf("how big is a long? %lu\n", sizeof(long));

  printf("how big is a long long? %lu\n", sizeof(long long));

  return 0;
}
