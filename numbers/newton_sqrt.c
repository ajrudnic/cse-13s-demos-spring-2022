#include <stdio.h>
#include <math.h>
#include <stdbool.h>

double newton_sqrt(double n) {
  double guess = n / 2;
  while (true) {
    guess = (guess + (n / guess)) / 2.0; 
    if (fabs((guess * guess) - n) < (1.0 / (1000 * 1000))) {
      return guess;
    }
  }

  return 0;
}

