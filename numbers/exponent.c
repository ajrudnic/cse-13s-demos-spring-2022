#include <stdio.h>
#include <math.h>

float to_the_third(float x) {
  // return x * x * x;
  return pow(x, 3);
}

float sine_of_x(float x) {
  return sin(x);
}

int main(void) {
  printf("16 to the third: %f\n", to_the_third(16.0));

  return 0;
}
